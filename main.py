import logging
import os
import sys
import asyncio

# https://docs.aiogram.dev/en/dev-3.x/index.html
from aiogram import Bot, Dispatcher, Router, types
from aiogram.enums import ParseMode
from aiogram import F
from aiogram.filters import Command, CommandStart, StateFilter
from aiogram.utils.keyboard import InlineKeyboardBuilder

from aiogram.types import (
    KeyboardButton,
    Message,
    ReplyKeyboardMarkup,
    InlineKeyboardButton,
    BufferedInputFile,
    BotCommand
)

from aiogram.fsm.storage.memory import MemoryStorage
from aiogram.fsm.context import FSMContext
from aiogram.fsm.state import State, StatesGroup

from configs import config

storage = MemoryStorage()
vote_router = Router()
dp = Dispatcher(storage=storage)
dp.include_router(vote_router)


# Initialize Bot instance with a default parse mode which will be passed to all API calls
bot = Bot(token=config.token, parse_mode=ParseMode.HTML)


# Menu Main
btn_participants = KeyboardButton(text='Учасники')
btn_vote = KeyboardButton(text='Проголосувати')
btn_results = KeyboardButton(text='Результати голосування')
menu_main = ReplyKeyboardMarkup(
    resize_keyboard=True,
    keyboard=[[btn_participants, btn_vote], [btn_results]],
    input_field_placeholder='Головне меню')


# Menu Vote
menu_start_vote = ReplyKeyboardMarkup(
    keyboard=[[KeyboardButton(text='start'),  KeyboardButton(text='cancel')]],
    resize_keyboard=True
)


# заглушка для імітації роботи з базою данних
participants = [
    ['1', 'Kalush Orchestra', 'Ukraine', 'ko.jpg', 'Stefania', 'https://www.youtube.com/watch?v=UiEGVYOruLk'],
    ['2', 'Måneskin', 'Italy', 'maneskin.jpg', 'Zitti E Buoni', 'https://www.youtube.com/watch?v=9mL6Cmkg2_A'],
    ['3', 'Duncan Laurence', 'Netherlands', 'duncan.jpg', 'Arcade', 'https://www.youtube.com/watch?v=R3D-r4ogr7s']]


# user likes manager dictionary: key = user_id, value = participant_id
USERS_LIKES_STORAGE = dict()


# participant likes manager
PARTICIPANTS_LIKES_STORAGE = {
    '1': 125,
    '2': 89,
    '3': 5
}


def get_participant_likes(participant_id) -> int:
    return PARTICIPANTS_LIKES_STORAGE[participant_id]


def increase_likes(participant_id, user_id) -> int:
    PARTICIPANTS_LIKES_STORAGE[participant_id] += 1
    USERS_LIKES_STORAGE[user_id] = participant_id
    return get_participant_likes(participant_id)


def check_user_vote(user_id) -> bool:
    return user_id in USERS_LIKES_STORAGE


# Машина станів для організації послідосних етапів голосування
# 1 - погодження з правилами
# 2 - вибір учасника
# 3 - підтвердження

class UserVote(StatesGroup):
    rules_agree = State()
    select_participant = State()
    choice_confirm = State()


# Обробник події відміни голосування. Cancel - команда або кнопка
@vote_router.message(F.text == "cancel")
@vote_router.message(Command("cancel"))
async def cancel_handler(message: Message, state: FSMContext):
    await state.clear()
    current_state = await state.get_state()
    if current_state is not None:
        logging.info('Cancelling state %r', current_state)

    await message.answer('Голосування перервано.', reply_markup=menu_main)


# Крок 1. Розпочати голосування
@vote_router.message(StateFilter(None), F.text == 'Проголосувати')
async def vote_start(message: Message, state: FSMContext):
    user_id = str(message.from_user.id)
    already_voted = check_user_vote(user_id)

    if not already_voted:
        await state.set_state(UserVote.rules_agree)
        await message.answer("Згідно правил, проголосувати можна лише ОДИН раз за ОДНОГО учасника.\n"
                             "Розпочати голосування ?", reply_markup=menu_start_vote)
    else:
        await message.answer("Згідно правил, проголосувати можна лише ОДИН раз.\n"
                             "Ви вже проголоcували. Дякуємо", reply_markup=menu_main)


# Крок 2. Користувач підтвердив голосування. Показуємо йому список учасників
@vote_router.message(UserVote.rules_agree, F.text == "start")
async def participant_vote(message: types.Message, state: FSMContext):
    # data = {'user_id': 12345677}
    data = await state.update_data(user_id=message.from_user.id)
    await state.set_state(UserVote.choice_confirm)

    vote_builder = InlineKeyboardBuilder()
    for participant in participants:
        btn_participant = InlineKeyboardButton(text=participant[2] + ' - ' + participant[1] + ' (проголосувати)',
                                               callback_data='vote-' + participant[0])
        vote_builder.add(btn_participant)

    vote_builder.adjust(1)
    await message.answer("Виберіть учасника, за якого хочете проголосувати", data=data, reply_markup=vote_builder.as_markup())


# Крок 3. Користувач обрав учасника. Зберігаємо результати голосування. Дякуємо за участь
@vote_router.callback_query(UserVote.choice_confirm)
async def choice_confirm(callback: types.CallbackQuery, state: FSMContext):
    data = await state.get_data()
    user_id = str(data['user_id'])
    participants_id = callback.data[5:]

    likes = increase_likes(participants_id, user_id)
    await state.clear()
    await bot.send_message(callback.from_user.id, text="Ваш голос враховано. В учасника голосів - " + str(likes),
                           reply_markup=menu_main)


# інформація про учасників євробачення
@dp.message(F.text == 'Учасники')
async def get_participants_list(message: Message):
    participants_list = []
    builder_participants = InlineKeyboardBuilder()

    for participant in participants:
        btn_participant = InlineKeyboardButton(text="{0} - {1} (слухати)".format(participant[2], participant[1]),
                                               callback_data='participant_info-' + participant[0])
        builder_participants.add(btn_participant)
    builder_participants.adjust(1)
    await message.answer("Детально про виконавця: ", reply_markup=builder_participants.as_markup())


# інформація про конкретного учасника
@dp.callback_query(F.data.startswith('participant_info-'))
async def get_participant_info(callback: types.CallbackQuery):
    participant_id = callback.data[len('participant_info-') :]
    for participant in participants:
        if participant[0] == participant_id:
            break

    await bot.send_message(callback.from_user.id, text="Інформація про учасника: " + participant[1])

    photo = os.path.abspath('img/participants/' + participant[3])
    with open(photo, 'rb') as image:
        photo = BufferedInputFile(image.read(), filename=photo)

    await bot.send_photo(callback.from_user.id, photo)
    await bot.send_message(callback.from_user.id, text='Кліп ' + participant[4] + '\n\n' + participant[5])


# результати голосування - список
@dp.message(F.text == 'Результати голосування')
async def get_participants_results(message: types.Message):
    participants_list = []
    for participant in participants:
        participants_list.append("{0:<}: {1:>4}".format(participant[1], PARTICIPANTS_LIKES_STORAGE[participant[0]]))

    participants_list = '\n'.join(participants_list)
    await message.answer(participants_list, reply_markup=menu_main)


# Обробка команди /start - запуск бота, вітання, функціонал
@dp.message(CommandStart())
async def command_start_handler(message: Message) -> None:
    await message.answer(f"Привіт, <b>{message.from_user.first_name} {message.from_user.last_name}</b>.\n"
                         "Я - <b>Бот Євробачення</b>. \n"
                         "Допомагаю провести глядацьке голосування.\n"
                         "І визначити переможця від глядачів\n", reply_markup=menu_main)


async def main() -> None:
    await dp.start_polling(bot)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    asyncio.run(main())